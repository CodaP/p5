/*
 * File: free1.c
 *
 * tests to make sure Mem_Free frees pointers properly and
 * handles invalid pointers
 *
 * Authors:
 *  Karl Foss: Section 1
 *  Coda Phillps: Section 1
 */
#include <assert.h>
#include <stdlib.h>
#include "mem.h"


/*
 *  main: tests that Mem_Free frees a pointer correctly
 *  and returns -1 when passed a bad pointer that is NULL
 *  or points to memory space that wasn't allocated
 *
 */
int main() 
{
    /* Create a memory space */
    assert(Mem_Init(8192) == 0);
    void* goodPointer,*nullPointer,*testPointer;
      
    /* Allocate a small amount of memory and point to it */ 
    testPointer = Mem_Alloc(9);
    /* Make sure Malloc allocated it correctly */
    assert(testPointer != NULL);
    /* Free the pointer and check it was done properly */
    assert(Mem_Free(testPointer) == 0);

    /* Allocate another chunk of memory and point to it */ 
    goodPointer = Mem_Alloc(457);
    /* Make sure Malloc allocated it correctly */
    assert(goodPointer != NULL);
    /* Make a null pointer */
    nullPointer = NULL;

    /* check that Mem_Free handles the nullPointer correctly */
    assert(Mem_Free(nullPointer) == -1);
    /* check that Mem_Free handles the invalid pointer correctly */
    assert(Mem_Free(goodPointer+1) == -1);

    return 0;
}
